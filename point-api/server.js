'use strict';

const express = require('express');
const mongoose = require('mongoose');

mongoose.connect('mongodb://point-db/app-points', {useNewUrlParser: true});

const pointSchema = new mongoose.Schema({
    _id: String,
    number: Number,
    last_operation : Date,
    last_accepted_operation : Date,
    last_operation_status: String,
});

const Point = mongoose.model('Point', pointSchema);

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();

let allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, UserId');

    // intercept OPTIONS method
    if ('OPTIONS' === req.method) {
        res.sendStatus(200);
    } else {
        next();
    }
};

app.use(allowCrossDomain);

app.get('/:id', async (req, res) => {
    let point = await Point.findOne({_id : req.params['id']});
    res.send(point);
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
