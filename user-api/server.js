'use strict';

const express = require('express');
const mongoose = require('mongoose');
const rhea = require('rhea');

mongoose.connect('mongodb://user-db/app-users', {useNewUrlParser: true});

const userSchema = new mongoose.Schema({
    _id: String,
    firstname: String,
    lastname: String,
    type: String
});

const User = mongoose.model('User', userSchema);

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();

let allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, UserId');

    // intercept OPTIONS method
    if ('OPTIONS' === req.method) {
        res.sendStatus(httpStatus.OK);
    } else {
        next();
    }
};

app.use(allowCrossDomain);

app.get('/', async (req, res) => {
    let users = await User.find();
    res.send(users);
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
